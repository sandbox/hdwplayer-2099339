<?php
function hdwplayer_configparser(){

	$results = db_query("SELECT * FROM {hdw_settings} t WHERE t.playerid = :id", array(':id' => filter_xss($_GET['id'])));
	$settings = array();		
	foreach ($results as $result){
		$settings[] = $result;
	}
	$autoplay 			= ($settings[0]->autoplay == 1) 			? "true" 	: "false" ;
	$skinmode 			= ($settings[0]->skinmode == "float") 		? "float" 	: "static" ;
	$playlistautoplay 	= ($settings[0]->playlistautoplay == 1) 	? "true" 	: "false" ;
	$playlistopen 		= ($settings[0]->playlistopen == 1) 		? "true" 	: "false" ;
	
	drupal_add_http_header('Content-Type', 'text/xml; charset=utf-8');	
	echo '<config>';
	echo '<autoStart>' . $autoplay . '</autoStart>';
	echo '<skinMode>' . $skinmode . '</skinMode>';
	echo '<buffer>' . $settings[0]->bufferspeed. '</buffer>';
	echo '<volumeLevel>' . $settings[0]->volumelevel . '</volumeLevel>';
	echo '<stretch>' . $settings[0]->stretch . '</stretch>';
	echo '<playlistAutoStart>' . $playlistautoplay . '</playlistAutoStart>';
	echo '<playlistOpen>' . $playlistopen . '</playlistOpen>';
	
	echo '<skinXml>' . $GLOBALS["base_url"] . '/?q=hdwskin' . '</skinXml>';
	$videoid = "";
	if(isset($_GET['vid'])){
		$videoid = "&videoid=" . $_GET['vid'];
	}
	echo '<playlistXml>' . $GLOBALS["base_url"] . '/?q=hdwplaylist' . $videoid . '</playlistXml>';
	echo '<vid>' . $settings[0]->vid . '</vid>';
	echo '<playlist>' . $settings[0]->playlist . '</playlist>';
	echo '<emailPhp>' . $GLOBALS["base_url"] . '/?q=hdw_email</emailPhp>';
	echo '</config>';
	exit();
}