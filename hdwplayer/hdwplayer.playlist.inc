<?php
function hdwplayer_playlistxml(){

	function playlistGeneration($result)
	{
		echo '<media>';
		echo '<id>' . $result->videoid . '</id>';
		echo '<type>' . $result->videotype . '</type>';
		echo '<video>' . $result->videourl . '</video>';
		if ($result->hdvideourl != "")
		{
			echo '<hd>' . $result->hdvideourl . '</hd>';
		}
		if ($result->streamer != "")
		{
			echo '<streamer>' . $result->streamer . '</streamer>';
		}
		if ($result->dvr != "")
		{
			echo '<dvr>' . $result->dvr . '</dvr>';
		}
		if ($result->token != "")
		{
			echo '<token>' . $result->token . '</token>';
		}
		
		if ($result->thumb != "")
		{
			echo '<thumb>' . $result->thumb . '</thumb>';
		}
		else
		{
			echo '<thumb>' . $GLOBALS["base_url"] . '/' . drupal_get_path('module', 'hdwplayer') . '/images/empty.png</thumb>';
		}
		
		if ($result->preview != "")
		{
			echo '<preview>' . $result->preview . '</preview>';
		}
		else
		{
			echo '<preview>' . $GLOBALS["base_url"] . '/' . drupal_get_path('module', 'hdwplayer') . '/images/empty.png</preview>';
		}
		
		echo '<title>' . strtoupper($result->videotitle) . '</title>';
		echo '</media>';
	}

	$results = db_query("SELECT * FROM {hdw_playlist} t WHERE t.isplaylist = :name", array(':name' => filter_xss($_GET['playlist']))); 
	if(isset($_GET['videoid']) && $_GET['videoid'] != "")
	{
		$single = db_query("SELECT * FROM {hdw_playlist} t WHERE t.videoid = :id", array(':id' => filter_xss($_GET['videoid'])));
		drupal_add_http_header('Content-Type', 'text/xml; charset=utf-8');	
		echo '<playlist>';
		
		foreach ($single as $result){
			playlistGeneration($result);
		}
		
		foreach ($results as $result){			
			 
			if($result->videoid != filter_xss($_GET['videoid']))
			{
				playlistGeneration($result);
			}
		}
		echo '</playlist>';
		exit();	
	}
	else
	{
		if($_GET['vid'] == 0)
		{
			drupal_add_http_header('Content-Type', 'text/xml; charset=utf-8');	
			echo '<playlist>';
			foreach ($results as $result){			
				 
				playlistGeneration($result);
			}
			echo '</playlist>';
			exit();	
		}
		else
		{
			$single = db_query("SELECT * FROM {hdw_playlist} t WHERE t.videoid = :id", array(':id' => filter_xss($_GET['vid'])));
			drupal_add_http_header('Content-Type', 'text/xml; charset=utf-8');	
			echo '<playlist>';
			
			foreach ($single as $result){
				playlistGeneration($result);
			}
			echo '</playlist>';
			exit();	
		}
	}
}