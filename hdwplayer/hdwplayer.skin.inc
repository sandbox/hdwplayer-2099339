<?php
function hdwplayer_getskinxml(){

	$results = db_query("SELECT * FROM {hdw_settings} t WHERE t.playerid = :id", array(':id' => filter_xss($_GET['id'])));
	
	drupal_add_http_header('Content-Type', 'text/xml; charset=utf-8');	
	foreach ($results as $result){
		$controlbar = ($result->controlbar == 1) ? "true" : "false";
		$playpause = ($result->playpause == 1) ? "true" : "false";
		$progressbar = ($result->progressbar == 1) ? "true" : "false";
		$timer = ($result->timer == 1) ? "true" : "false";
		$share = ($result->share == 1) ? "true" : "false";
		$volume = ($result->volume == 1) ? "true" : "false";
		$playdock = ($result->playdock == 1) ? "true" : "false";
		$fullscreen = ($result->fullscreen == 1) ? "true" : "false";
		$playlistdock = ($result->playlistdock == 1) ? "true" : "false";		
		 
		echo "<skin>";
		echo "<controlbar><display>" . $controlbar . "</display></controlbar>";
		echo "<playpause><display>" . $playpause . "</display></playpause>";
		echo "<progressbar><display>" . $progressbar . "</display></progressbar>";
		echo "<timer><display>" . $timer . "</display></timer>";
		echo "<share><display>" . $share . "</display></share>";
		echo "<volume><display>" . $volume . "</display></volume>";
		echo "<playdock><display>" . $playdock . "</display></playdock>";
		echo "<fullscreen><display>" . $fullscreen . "</display></fullscreen>";
		echo "<videogallery><display>" . $playlistdock . "</display></videogallery>";
		echo "</skin>";
	}
	exit();	
}