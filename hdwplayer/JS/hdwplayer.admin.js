jQuery(document).ready(function (){
	$hdw = jQuery.noConflict();
	$hdw.ajax({
		
		url:Drupal.settings.basePath + "?q=playerdetails",
	    data:
	    	{
				drupaltoken:mytoken
			},
	    dataType: "json",
		success: function(data){
				
			var players = '<table style="width:100%;">';
			var vid, playlist;
			players += '<tr><th>Player-Id</th>		<th>Video-Id</th>	<th>Playlist Name</th>';
			players += '<th>Width</th>	<th>Height</th>	<th>Udpate</th>			<th>Delete</th>	</tr>';
	    	
			if(data.length == 0)
    		{
	    		players += '<tr id="emptyplayer"><td align="center" colspan="7">Here is no players yet</td></tr>';
    		}
	    	else
    		{
		    	for(i=0;i<data.length;i++)
		    	{
		    		vid = (data[i].vid == 0) ? "-" : data[i].vid;
		    		playlist = (data[i].playlist == "") ? "-" : data[i].playlist;
		    		players += '<tr id="pl' + data[i].playerid + '"><td id="player' + data[i].playerid + '">' + data[i].playerid + '</td>';
		    		players += '<td id="video' + data[i].playerid + '">' + vid + '</td>';
		    		players += '<td id="plist' + data[i].playerid + '">' + playlist + '</td>';
		    		players += '<td id="width' + data[i].playerid + '">' + data[i].width + '</td>';
		    		players += '<td id="height' + data[i].playerid + '">' + data[i].height + '</td>';
		    		players += '<td><input id="updateplayer' + data[i].playerid + '" type="button" value="Udpate" style="cursor:pointer; border:1px solid #ccc; height:20px; width:80px;"/></td>';
		    		players += '<td><input id="deleteplayer' + data[i].playerid + '" type="button" value="Delete" style="cursor:pointer; border:1px solid #ccc; height:20px; width:80px;"/></td>';
		    		players += '</tr>';
		    	}
			}
	    	
			players += '</table>';
	    	$hdw('#playertotalcontent').append(players);
		}
		
	});
	
	$hdw.ajax({
		
		url:Drupal.settings.basePath + "?q=hdwgallerysettings",
	    data:
	    	{
				drupaltoken:mytoken
			},
	    dataType: "json",
		success: function(data){
				
			var gallery = '<table style="width:100%;">';
			var vid, playlist;
			gallery += '<tr><th>Gallery-Id</th>		<th>Name</th>	<th>No.of Rows</th>	<th>No.of Columns</th>';
			gallery += '<th>Display Limit</th>	<th>Thumb Width</th>	<th>Thumb Height</th>';	
			gallery += '<th>Thumb Space</th>	<th>Udpate</th>		<th>Delete</th>	</tr>';
	    	
			if(data.length == 0)
    		{
	    		gallery += '<tr id="emptygallery"><td align="center" colspan="10">Here is no gallery yet</td></tr>';
    		}
	    	else
    		{
		    	for(i=0;i<data.length;i++)
		    	{
		    		gallery += '<tr id="gl' + data[i].gid + '"><td id="gallery' + data[i].gid + '">' + data[i].gid + '</td>';
		    		gallery += '<td id="glname' + data[i].gid + '">' + data[i].name + '</td>';
		    		gallery += '<td id="glrows' + data[i].gid + '">' + data[i].rows + '</td>';
		    		gallery += '<td id="glcolumns' + data[i].gid + '">' + data[i].columns + '</td>';
		    		gallery += '<td id="gllimit' + data[i].gid + '">' + data[i].limits + '</td>';
		    		gallery += '<td id="glwidth' + data[i].gid + '">' + data[i].thumbwidth + '</td>';
		    		gallery += '<td id="glheight' + data[i].gid + '">' + data[i].thumbheight + '</td>';
		    		gallery += '<td id="glspace' + data[i].gid + '">' + data[i].thumbspace + '</td>';
		    		gallery += '<td><input id="updategallery' + data[i].gid + '" type="button" value="Udpate" style="cursor:pointer; border:1px solid #ccc; height:20px; width:80px;"/></td>';
		    		gallery += '<td><input id="deletegallery' + data[i].gid + '" type="button" value="Delete" style="cursor:pointer; border:1px solid #ccc; height:20px; width:80px;"/></td>';
		    		gallery += '</tr>';
		    		$hdw('#playergallery').append('<option value="' + data[i].name + '">' + data[i].name + '</option>');
		    	}
			}
	    	
			gallery += '</table>';
	    	$hdw('#gallerytotalcontent').html(gallery);
		}
		
	});

	$hdw.ajax({
		
		url:Drupal.settings.basePath + "?q=getplaylist",
	    data:{token:mytoken},
	    dataType: "json",
		success: function(data){
			for (i=0;i<data.length;i++)
			{
				$hdw('#playerplaylist, #videoplaylist').append('<option value="' + data[i].playlistname + '">' + data[i].playlistname + '</option>');
			}
		}
		
	});

	$hdw.ajax({
		
		url:Drupal.settings.basePath + "?q=getallvideolists",
	    data:{drupaltoken:mytoken},
	    dataType: "json",
		success: function(data){	    	
	    	var tempvideotable = '<table style="width:100%;">';
	    	tempvideotable += '<tr><th><input id="selectallvideos" type="checkbox" /></th>		<th>Video-Id</th>	<th>Video-Title</th>';
	    	tempvideotable += '<th>Playlist</th>	<th>Udpate</th>			<th>Delete</th>	</tr>';
	    	
	    	if(data.length == 0)
    		{
	    		tempvideotable += '<tr id="emptyvideos"><td align="center" colspan="7">Here is no videos yet</td></tr>';
    		}
	    	else
    		{
	    		for(i=0;i<data.length;i++)
		    	{
			    	tempvideotable += '<tr id="v' + data[i].videoid + '"><td><input id="videoid' + data[i].videoid + '" type="checkbox" /></td>';
			    	tempvideotable += '<td>' + data[i].videoid + '</td>	<td>' + data[i].videotitle + '</td>';
			    	tempvideotable += '<td id="videoplaylist' + data[i].videoid + '">' + data[i].isplaylist + '</td>	';
			    	tempvideotable += '<td><input id="updatevideo' + data[i].videoid + '" type="button" value="Udpate" style="cursor:pointer; border:1px solid #ccc; height:20px; width:80px;"/></td>';
			    	tempvideotable += '<td><input id="deletevideo' + data[i].videoid + '" type="button" value="Delete" style="cursor:pointer; border:1px solid #ccc; height:20px; width:80px;"/></td>';
			    	tempvideotable += '</tr>';
			    	$hdw('#singlevideoid').append('<option value="' + data[i].videoid + '">' + data[i].videoid + '</option>');
		    	}
    		}    	
	    	
	    	tempvideotable += '</table>';
	    	$hdw('#deletevideos').before(tempvideotable);	    	
		}
		
	});

	$hdw.ajax({
		
		url:Drupal.settings.basePath + "?q=getallplaylists",
	    data:{drupaltoken:mytoken},
	    dataType: "json",
		success: function(data){	    	
	    	var tempvideotable = '<table style="width:100%;">';
	    	tempvideotable += '<tr><td style="border:none;" colspan="4"><span style="font-size:12px;"><b>Note:</b> If you delete playlist, all videos that are associated with playlist will be deleted...</span></td></tr>';
	    	tempvideotable += '<tr><th>P-Id</th>		<th>PlaylistName</th>	<th>Update</th>		<th>Delete</th></tr>';
	    	
	    	if(data.length == 0)
    		{
	    		tempvideotable += '<tr id="emptyplaylist"><td align="center" colspan="4">Here is no playlists yet</td></tr>';
    		}
	    	else
    		{
	    		for(i=0;i<data.length;i++)
		    	{
			    	tempvideotable += '<tr id="p' + data[i].playlistid + '"><td>' + data[i].playlistid + '</td>';
			    	tempvideotable += '<td id="playlist' + data[i].playlistid + '">' + data[i].playlistname + '</td>';
			    	tempvideotable += '<td><input id="updateplaylist' + data[i].playlistid + '" type="button" value="Udpate" style="cursor:pointer; border:1px solid #ccc; height:20px; width:80px;"/></td>';
			    	tempvideotable += '<td><input id="deleteplaylist' + data[i].playlistid + '" type="button" value="Delete" style="cursor:pointer; border:1px solid #ccc; height:20px; width:80px;"/></td>';
			    	tempvideotable += '</tr>';
		    	}
    		}  	    	
	    	
	    	tempvideotable += '</table>';
	    	$hdw('#playlisttotalcontent').html(tempvideotable);
		}
		
	});	
	
	$hdw('#settingstable #settingsheadingrow td').click(function (){
		var id = this.id;
		$hdw('#settingstable td').css({"font-weight":"normal", "background":"#fff"});
		$hdw(this).css({
			"font-weight":"bold",
			"background":"#fcfcf5"
		});

		switch (id)
		{
		case 'playersettings':
			$hdw('#playerlists, #playertotalcontent').css("display","block");
			$hdw('#gallerytotalcontent, #hdwgallerydiv, #insertvideo, #insertplaylist, #documentationdiv, #videototalcontent, #playlisttotalcontent, #updateplaylistcontent, #saveplayer, #cancelplayer, #playerdetails, #updateplayerbutton, #cancelplayer_update').css("display","none");
			break;

		case 'videosettings':
			$hdw('#insertvideo, #videototalcontent').css("display","block");
			$hdw('#videodetails :text').val("");
			$hdw('#videotype').val("direct");
			$hdw('#videoplaylist').val("playlist");
			$hdw('#videotitle').val("").attr('class','');
			$hdw('#gallerytotalcontent, #hdwgallerydiv, #playerlists, #insertplaylist, #documentationdiv, #playlisttotalcontent, #videodetails, #updateplaylistcontent, #playertotalcontent').css("display","none");
			break;

		case 'playlistsettings':
			$hdw('#insertplaylist, #playlisttotalcontent').css("display","block");
			$hdw('#gallerytotalcontent, #hdwgallerydiv, #playerlists, #insertvideo, #documentationdiv, #videototalcontent,  #playlistdetails, #updateplaylistcontent, #playertotalcontent').css("display","none");
			break;
			
		case 'gallerysettings':
			$hdw('#gallerytotalcontent, #hdwgallerydiv').css("display","block");
			$hdw('#gallerydetails, #documentationdiv, #playerlists, #insertvideo, #insertplaylist, #videototalcontent, #playlisttotalcontent, #updateplaylistcontent, #playertotalcontent').css("display","none");
			break;

		case 'documentation':
			$hdw('#documentationdiv').css("display","block");
			$hdw('#gallerytotalcontent, #hdwgallerydiv, #playerlists, #insertvideo, #insertplaylist, #videototalcontent, #playlisttotalcontent, #updateplaylistcontent, #playertotalcontent').css("display","none");
			break;
		}
	});
	
	$hdw('#saveplayer, #updateplayerbutton').click(function (){

		var id = this.id;
		var skinlist = "";
		var videoplaytype, playlistname, videoid;
		var width = $hdw('#width').val();
		var height = $hdw('#height').val();
		var buffer = $hdw('#buffer').val();
		var volumelevel = $hdw('#volumelevel').val();
		var gallery = $hdw('#playergallery').val() ;
		var gallerydisplay = $hdw('#galleryoption').val() ;
		$hdw('.skinlist').each(function (){
			skinlist += '`' + this.value;
		});
		
		if($hdw('#radiosingle').is(":checked"))
		{
			if($hdw('#singlevideoid').val() == 'video') 
			{ 
				$hdw('#hdw_status').text("Please Select Video Id").css({"background":"#FFcABA","color":"#D8000C"}).show().fadeOut(3000);
				$hdw('body').scrollTop(0);
			} 
			else 
			{ 				
				videoid = $hdw('#singlevideoid').val();
				videoplaytype = $hdw('#radiosingle').val();
				saveplayer(gallerydisplay, gallery, id, width, height, buffer, volumelevel, skinlist, videoplaytype, videoid, "");
			}
		}
		else if($hdw('#radioplaylist').is(":checked"))
		{
			if($hdw('#playerplaylist').val() == 'playlist') 
			{ 
				$hdw('#hdw_status').text("Please Select Playlist").css({"background":"#FFcABA","color":"#D8000C"}).show().fadeOut(3000);
				$hdw('body').scrollTop(0);
			}
			else
			{ 
				playlistname = $hdw('#playerplaylist').val();
				videoplaytype = $hdw('#radioplaylist').val();
				saveplayer(gallerydisplay, gallery, id, width, height, buffer, volumelevel, skinlist, videoplaytype, 0, playlistname);
			}
		}
		
	});
	
	$hdw('#updateplaylistbutton').click(function (){
		
		if($hdw.trim($hdw('#updateplaylistname').val()) != "")
		{
			var playlistid = $hdw('#updateplaylistname').attr("class").split("`");
			var playlistname = $hdw.trim($hdw('#updateplaylistname').val());
				$hdw.ajax({
					
					url:Drupal.settings.basePath + "?q=getplaylist",
				    data:
				    	{
							token:mytoken, 
							playlistid:playlistid[0],
							oldplaylistname:playlistid[1],
							playlistname:playlistname
						},
					success: function(data){
						if(playlistname == playlistid[1])
						{
							$hdw('#updateplaylistcontent').hide();
							$hdw('#insertplaylist, #playlisttotalcontent').show();
							$hdw('#playlist' + playlistid).text($hdw('#updateplaylistname').val());	
							
							$hdw('#videoplaylist option').each(function(){
							    if (this.value == playlistid[1]) {
							    	$hdw(this).replaceWith("<option value='" + playlistname + "'>" + playlistname + "</option>");
							    }
							});
							
							$hdw('#videototalcontent table td').each(function (){
								if($hdw(this).text() == playlistid[1])
								{
									$hdw(this).text(playlistname);
								}
							});
							
							$hdw('#playertotalcontent table td').each(function (){
								if($hdw(this).text() == playlistid[1])
								{
									$hdw(this).text(playlistname);
								}
							});
							
							$hdw('#playerplaylist option').each(function(){
							    if (this.value == playlistid[1]) {
							    	$hdw(this).replaceWith("<option value='" + playlistname + "'>" + playlistname + "</option>");
							    }
							});
							$hdw('#hdw_status').text("Playlist Updated Without Any Changes").css({"background":"#DFF2BF","color":"#4F8A10"}).show().fadeOut(3000);
						}
						else if(data != 'Already exists')
						{
							$hdw('#updateplaylistcontent').hide();
							$hdw('#insertplaylist, #playlisttotalcontent').show();
							$hdw('#playlist' + playlistid).text($hdw('#updateplaylistname').val());	
							
							$hdw('#videoplaylist option').each(function(){
							    if (this.value == playlistid[1]) {
							    	$hdw(this).replaceWith("<option value='" + playlistname + "'>" + playlistname + "</option>");
							    }
							});
							
							$hdw('#videototalcontent table td').each(function (){
								if($hdw(this).text() == playlistid[1])
								{
									$hdw(this).text(playlistname);
								}
							});
							
							$hdw('#playertotalcontent table td').each(function (){
								if($hdw(this).text() == playlistid[1])
								{
									$hdw(this).text(playlistname);
								}
							});
							
							$hdw('#playerplaylist option').each(function(){
							    if (this.value == playlistid[1]) {
							    	$hdw(this).replaceWith("<option value='" + playlistname + "'>" + playlistname + "</option>");
							    }
							});
							$hdw('#hdw_status').text("Playlist Updated Successfully").css({"background":"#DFF2BF","color":"#4F8A10"}).show().fadeOut(3000);
						}
						else
						{
							$hdw('#hdw_status').text(playlistname + " is already exist.").css({"background":"#FFcABA","color":"#D8000C"}).show().fadeOut(3000);
							$hdw('#updateplaylistname').focus();
						}
					}				
				});			
		}
		else
		{
			$hdw('#updateplaylistname').focus();
			$hdw('#hdw_status').text("Please Enter Playlist Name").css({"background":"#FFcABA","color":"#D8000C"}).show().fadeOut(3000);
		}
	});
	
	$hdw('#addplayer').click(function (){		
		$hdw('#playerdetails, #saveplayer, #cancelplayer').show();
		$hdw('#playertotalcontent, #updateplayerbutton, #cancelplayer_update').hide();	
		$hdw('#width').attr('class','');
		
		var skinlist = ['static', 'fill', 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1];
		var i=0;
		$hdw('.skinlist').each(function (){
			$hdw(this).val(skinlist[i]);
			i++;
		});
		$hdw('#width').val(400);
		$hdw('#height').val(250);
		$hdw('#buffer').val(3);
		$hdw('#volumelevel').val(50);
		$hdw('#radiosingle').attr('checked', false);
		$hdw('#radioplaylist').attr('checked', true);
		$hdw('#playerplaylist, #playlistopen, #playlistautoplay').attr('disabled',false);
		$hdw('#singlevideoid').val('video').attr('disabled',true);
		$hdw('#playerplaylist').val('playlist');
		$hdw('#playergallery, #galleryoption').val('none').attr('disabled',false);
	});
	
	$hdw('#cancelplayer').click(function (){		
		$hdw('#playerdetails, #saveplayer, #cancelplayer').hide();
		$hdw('#playertotalcontent').show();
	});
	
	$hdw('#cancelplayer, #cancelplayer_update').click(function (){		
		$hdw('#playerdetails, #saveplayer, #cancelplayer, #updateplayerbutton, #cancelplayer_update').hide();
		$hdw('#playertotalcontent').show();
	});
	
	$hdw('#addplaylist').click(function (){		
		$hdw('#playlistdetails').show();
		$hdw('#playlisttotalcontent').hide();
	});
	
	$hdw('#cancelvideo, #cancelvideo_update').click(function (){
		$hdw('#videodetails').hide();
		$hdw('#videototalcontent').show();
		$hdw('#videodetails :text').val("");
		$hdw('#videotype').val("direct");
		$hdw('#videoplaylist').val("playlist");
		$hdw('#videotitle').val("").attr('class','');
	});
	
	$hdw('#cancelplaylist').click(function (){
		$hdw('#playlistdetails').hide();
		$hdw('#playlisttotalcontent').show();
	});
	
	$hdw('#cancelupdateplaylist').click(function (){
		$hdw('#updateplaylistcontent').hide();
		$hdw('#playlisttotalcontent, #insertplaylist').show();
	});
	
	var delids = [];
	$hdw('#videototalcontent').delegate('#deletevideos','click',function (){
			
		$hdw("#videototalcontent :checkbox").each(function(){			
		    if($hdw(this).is(':checked'))
		    {
		    	delids.push(this.id);
		    }		    
		});
		var ids, temp;
		if(delids.length == 0)
		{
			$hdw('#hdw_status').text("Please Check Atleast One Checkbox").css({"background":"#FFcABA","color":"#D8000C"}).show().fadeOut(3000);
			$hdw('body').scrollTop(0);
		}
		else
		{
			var confirmmsg = confirm("Do you really want to perform this action?");
		    if (confirmmsg == true) 
		    {
				for(i=0;i<delids.length;i++)
				{
					if(delids[i] != 'selectallvideos')
					{
						temp = delids[i].substr(7);
						ids += "`" + temp;
						$hdw('#v' + temp).remove();
					}			
				}
		
				$hdw.ajax({						
					url:Drupal.settings.basePath + "?q=deletevideoandplaylist",
					data:
						{
							drupaltoken:mytoken,
							idgroup:ids
						},
					dataType: "json",
					success: function (data){
							
						$hdw('#selectallvideos').attr('checked',false);
						$hdw('#singlevideoid').empty().append('<option value="video" selected="selected">Select</option>');
						
						for(i=0;i<data.length;i++)
						{
							$hdw('#singlevideoid').append('<option value="' + data[i].videoid + '">' + data[i].videoid + '</option>');
						}
					}
				});			
		    }
			delids = [];
		}
	});
	
	$hdw('#videototalcontent').delegate('table tr:first :checkbox','click',function (){
        if ($hdw(this).is(':checked')) {
        	$hdw('#videototalcontent table :checkbox').attr('checked', true);

        } else {
        	$hdw('#videototalcontent table :checkbox').attr('checked', false);
        }
    });
	
	$hdw('#videototalcontent').delegate('table tr :checkbox','click',function (){
        if(this.id != 'selectallvideos') { $hdw('#selectallvideos').attr("checked",false); }
    });
	
	$hdw('#playertotalcontent').delegate('table td :button','click',function (){
		
		if(this.id.substr(0,5) == "delet")
		{
			var confirmmsg = confirm("Do you really want to delete this player?");
		    if (confirmmsg == true) 
		    {
				var action = this.id.substr(12);
				$hdw('#pl' + action).remove();
				$hdw.ajax({				
					
					url:Drupal.settings.basePath + "?q=playerdetails",
					data:
						{
							drupaltoken:mytoken,
							deleteplayerid:this.id.substr(12)
						},
					dataType: "json",
					success: function (data){
						if(data.length == 0) { $hdw('#emptyplayer').show(); }
					}
				});
		    }
		}	
		else if(this.id.substr(0,5) == "updat")
		{			
			var playerid = this.id.substr(12);
			$hdw('#playerdetails, #updateplayerbutton, #cancelplayer_update').show();
			$hdw('#playertotalcontent').hide();
			$hdw.ajax({				
				
				url:Drupal.settings.basePath + "?q=playerdetails",
				data:
					{
						drupaltoken:mytoken,
						getplayerdata_id:playerid
					},
				dataType: "json",
				success: function (data){
					$hdw('#width').attr('class',playerid);
					var skinlist = [data[0].skinmode, data[0].stretch, data[0].playlistopen, data[0].playlistautoplay, data[0].autoplay, data[0].controlbar, data[0].playpause, data[0].progressbar, data[0].timer, data[0].share, data[0].volume, data[0].fullscreen, data[0].playdock, data[0].playlistdock];
					var i=0;

					$hdw('.skinlist').each(function (){
						$hdw(this).val(skinlist[i]);
						i++;
					});

					if(data[0].videodesire == 'playlist')
					{
						$hdw('#radiosingle').attr('checked', false);
						$hdw('#radioplaylist').attr('checked', true);
						$hdw('#playerplaylist, #playlistopen, #playlistautoplay').attr('disabled',false);
						$hdw('#singlevideoid').attr('disabled',true);
						$hdw('#playerplaylist').val(data[0].playlist);
					}
					else
					{
						$hdw('#radiosingle').attr('checked', true);
						$hdw('#radioplaylist').attr('checked', false);
						$hdw('#playerplaylist, #playlistopen, #playlistautoplay').attr('disabled',true);
						$hdw('#singlevideoid').attr('disabled',false);
						$hdw('#singlevideoid').val(data[0].vid);
					}

		    		if(data[0].gallery != 'none') { $hdw('#playergallery').val(data[0].gallery); }
		    		$hdw('#galleryoption').val(data[0].gallerydisplay);
					$hdw('#width').val(data[0].width).focus();
					$hdw('#height').val(data[0].height);
					$hdw('#buffer').val(data[0].bufferspeed);
					$hdw('#volumelevel').val(data[0].volumelevel);
					
				}
			});
		}
	});

	$hdw('#videototalcontent').delegate('table td :button','click',function (){
		
		if(this.id.substr(0,5) == "delet" && this.id != 'deletevideos')
		{
			var confirmmsg = confirm("Do you really want to delete this video?");
		    if (confirmmsg == true) 
		    {
				var action = this.id.substr(11);
				$hdw('#v' + action).remove();
				$hdw.ajax({				
					
					url:Drupal.settings.basePath + "?q=deletevideoandplaylist",
					data:
						{
							drupaltoken:mytoken,
							videoid:this.id.substr(11)
						},
					dataType: "json",
					success: function (data){
						if(data.length == 0) { $hdw('#emptyvideos').show(); }
					}
				});
				$hdw('#singlevideoid option').each(function(){
				    if (this.value == action) {
				    	$hdw(this).remove();
				    }
				});
		    }
		}
		else if(this.id.substr(0,5) == "updat" && this.id != 'deletevideos')
		{				
			$hdw('#hdwfile').val('');
			$hdw('.hdwupload').attr('disabled', true);	
			$hdw.ajax({			
				
				url:Drupal.settings.basePath + "?q=sendvideodetails",
				data:
					{
						drupaltoken:mytoken,
						updatevideoid:this.id.substr(11)
					},
				dataType: "json",
				success: function (data){
					$hdw('#videotitle').val(data[0].videotitle).attr('class',data[0].videoid).focus();
					$hdw('#videotype').val(data[0].videotype);
					$hdw('#videourl').val(data[0].videourl);
					$hdw('#videohdurl').val(data[0].hdvideourl);
					(data[0].videotype == 'youtube' || data[0].videotype == 'dailymotion') ? $hdw('#videopreview').val(data[0].preview).attr('disabled',true) :  $hdw('#videopreview').val(data[0].preview);
					(data[0].videotype == 'youtube' || data[0].videotype == 'dailymotion') ? $hdw('#videothumb').val(data[0].thumb).attr('disabled',true) :  $hdw('#videothumb').val(data[0].thumb);
					$hdw('#videostreamer').val(data[0].streamer);
					$hdw('#videotoken').val(data[0].token);
					$hdw('#videodvr').val(data[0].dvr);
					$hdw('#videoplaylist').val(data[0].isplaylist);
				}
			});

			$hdw('#videototalcontent').hide();
			$hdw('#videodetails, #updatebuttons').show();
			$hdw('#savebuttons').hide();
		}
	});

	$hdw('#playlisttotalcontent').delegate('table td :button','click',function (){
		var playlistname = $hdw('#playlist' + this.id.substr(14)).text();
		if(this.id.substr(0,5) == "delet")
		{
		    var confirmmsg = confirm("All videos that are associated with playlist will be deleted. Do you really want to delete this playlist?");
		    if (confirmmsg == true) 
		    {
		    	var action = this.id.substr(14);
				$hdw('#p' + action).remove();
				$hdw.ajax({		
					
					url:Drupal.settings.basePath + "?q=deletevideoandplaylist",
					data:
						{
							drupaltoken:mytoken,
							playlistname:playlistname,
							playlistid:this.id.substr(14)
						},
					dataType: "json",
					success: function (data){
	
						if(data.length == 0) { $hdw('#emptyplaylist').show(); }
						$hdw('#singlevideoid').empty().append('<option value="video" selected="selected">Select</option>');
						for(i=0;i<data.length;i++)
						{
							$hdw('#singlevideoid').append('<option value="' + data[i].videoid + '">' + data[i].videoid + '</option>');
						}
							
						$hdw('#videoplaylist option').each(function(){
						    if (this.value == playlistname) {
						    	$hdw(this).remove();
						    }
						});
						
						$hdw('#playerplaylist option').each(function(){
						    if (this.value == playlistname) {
						    	$hdw(this).remove();
						    }
						});
						
						$hdw('#videototalcontent table td').each(function (){
							if($hdw(this).text() == playlistname)
							{
								$hdw(this).closest('tr').remove()
							}
						});
					}
				});		
		    }			
		}	
		else if(this.id.substr(0,5) == "updat")
		{
			var playlistname = $hdw('#playlist' + this.id.substr(14)).text();
			$hdw('#insertplaylist, #playlisttotalcontent').hide();
			$hdw('#updateplaylistcontent').show();
			$hdw('#updateplaylistname').val(playlistname).focus();
			$hdw('#updateplaylistname').attr('class',(this.id.substr(14) + "`" + playlistname));
		}
	});
	
	$hdw('#addvideo').click(function (){
		
		$hdw('#videototalcontent').hide();
		$hdw('#videodetails, #savebuttons').show();
		$hdw('#updatebuttons').hide();
		$hdw('#videodetails :text, #hdwfile').val("");
		$hdw('#videotype').val("direct");
		$hdw('.hdwupload').attr("disabled", true).css('opacity', '0.5');
		$hdw('#videoplaylist').val("playlist");
		$hdw('#videotitle').val("").attr('class','');
		
	});
	
	$hdw('#saveplaylist').click(function (){
		if($hdw.trim($hdw('#playlistname').val()) != "")
		{
			$hdw.ajax({
				url:Drupal.settings.basePath + "?q=playlistdata",
				data:
					{
						playlistvalue:$hdw.trim($hdw('#playlistname').val()), 
						token:mytoken
					},
				dataType: "json",
				success: function (data){	
					if(data == "Already exists")
					{
						$hdw('#hdw_status').text($hdw('#playlistname').val() + " is already exist.").css({"background":"#FFcABA","color":"#D8000C"}).show().fadeOut(3000);
						$hdw('#playlistname').focus();
					}
					else
					{
						var lastindex = data.length - 1;
						$hdw('#playerplaylist, #videoplaylist').append('<option value="' + data[lastindex].playlistname + '">' + data[lastindex].playlistname + '</option>');
						$hdw('#playlistname').val("");
						var tempvideotable = '<tr id="p' + data[lastindex].playlistid + '"><td>' + data[lastindex].playlistid + '</td>	<td id="playlist' + data[lastindex].playlistid + '">' + data[lastindex].playlistname + '</td>';
				    	tempvideotable += '<td><input id="updateplaylist' + data[lastindex].playlistid + '" type="button" value="Udpate" style="cursor:pointer; border:1px solid #ccc; height:20px; width:80px;"/></td>';
				    	tempvideotable += '<td><input id="deleteplaylist' + data[lastindex].playlistid + '" type="button" value="Delete" style="cursor:pointer; border:1px solid #ccc; height:20px; width:80px;"/></td>';
				    	tempvideotable += '</tr>';
				    	$hdw('#playlisttotalcontent table').append(tempvideotable);
						$hdw('#playlisttotalcontent').show();
						$hdw('#playlistdetails, #emptyplaylist').hide();
						$hdw('#hdw_status').text("Playlist Created Successfully").css({"background":"#DFF2BF","color":"#4F8A10"}).show().fadeOut(3000);
					}	
				}
			});
		}
		else
		{
			$hdw('#playlistname').focus();
			$hdw('#hdw_status').text("Please Enter Playlist Name").css({"background":"#FFcABA","color":"#D8000C"}).show().fadeOut(3000);
		}
	});
	
	function inserturl(id, mytoken, title, isplaylist, url, hdurl, type, thumb, preview, streamer, token, dvr)
	{
		$hdw.ajax({
			url:Drupal.settings.basePath + "?q=sendvideodetails",
			data:
				{
					videoid_update:$hdw('#videotitle').attr('class'),
					actionid:id,
					title:title,
					type:type,
					url:url,
					hdurl:hdurl,
					preview:preview,
					thumb:thumb,
					streamer:streamer,
					token:token,
					dvr:dvr,
					isplaylist:isplaylist,
					drupaltoken:mytoken
				},
			dataType: "json",
			success: function (data){
				$hdw('#videotitle, #videourl, #videohdurl, #videopreview, #videothumb, #videostreamer, #videotoken').val("");
				$hdw('#videotype').val("direct");
				$hdw('#videoplaylist').val("playlist");
				$hdw('#videodvr').attr("checked", false);
				
				if (id == 'updatevideobutton')
				{
					var index; 
					var row = $hdw('#videotitle').attr('class');
					for(i=0;i<data.length;i++)
					{
						if(row == data[i].videoid) { index = i; }
					}
					var tempvideotable = '<tr id="v' + data[index].videoid + '"><td><input id="videoid' + data[index].videoid + '" type="checkbox" /></td>';
					tempvideotable += '<td>' + data[index].videoid + '</td>	<td>' + data[index].videotitle + '</td>	';
			    	tempvideotable += '<td id="videoplaylist' + data[index].videoid + '">' + data[index].isplaylist + '</td>	';
			    	tempvideotable += '<td><input id="updatevideo' + data[index].videoid + '" type="button" value="Udpate" style="cursor:pointer; border:1px solid #ccc; height:20px; width:80px;"/></td>';
			    	tempvideotable += '<td><input id="deletevideo' + data[index].videoid + '" type="button" value="Delete" style="cursor:pointer; border:1px solid #ccc; height:20px; width:80px;"/></td>';
			    	tempvideotable += '</tr>';
					$hdw('#v' + row).replaceWith(tempvideotable);
					$hdw('#videotitle').attr('class','');
					$hdw('#hdw_status').text("Video Updated Successfully").css({"background":"#DFF2BF","color":"#4F8A10"}).show().fadeOut(3000);
					$hdw('body').scrollTop(0);
				}
				else if(id == 'savevideo')
				{
					var index = data.length - 1;
					var tempvideotable = '<tr id="v' + data[index].videoid + '"><td><input id="videoid' + data[index].videoid + '" type="checkbox" /></td>';
					tempvideotable += '<td>' + data[index].videoid + '</td>	<td>' + data[index].videotitle + '</td>	';
			    	tempvideotable += '<td id="videoplaylist' + data[index].videoid + '">' + data[index].isplaylist + '</td>	';
			    	tempvideotable += '<td><input id="updatevideo' + data[index].videoid + '" type="button" value="Udpate" style="cursor:pointer; border:1px solid #ccc; height:20px; width:80px;"/></td>';
			    	tempvideotable += '<td><input id="deletevideo' + data[index].videoid + '" type="button" value="Delete" style="cursor:pointer; border:1px solid #ccc; height:20px; width:80px;"/></td>';
			    	tempvideotable += '</tr>';
					$hdw('#videototalcontent table').append(tempvideotable);
					$hdw('#hdw_status').text("Video Created Successfully").css({"background":"#DFF2BF","color":"#4F8A10"}).show().fadeOut(3000);
					$hdw('body').scrollTop(0);
					$hdw('#singlevideoid').append('<option value="' + data[index].videoid + '">' + data[index].videoid + '</option>');
				}		
				$hdw('#videototalcontent').show();
				$hdw('#videodetails, #emptyvideos').hide();
				
			}
		});
	}
	
	var options = { 
	    beforeSend: function() 
	    {
	        $hdw('#uploadstatus').show();
	    },
	    complete: function(response) 
	    {
	    	$hdw('#uploadstatus').hide();
			var result = response.responseText;
			if(result == 'error')
			{
				$hdw('#hdw_status').text("Make Sure Its Correct Video").css({"background":"#FFcABA","color":"#D8000C"}).show().fadeOut(3000);
				$hdw('body').scrollTop(0);
			}
			else
			{
				$hdw('#videourl').val(result);
			}
	    }
	}; 
	$hdw("#hdwupload_form").ajaxForm(options);
	
	$hdw('#savevideo, #updatevideobutton').click(function (){
		
		var id = this.id;
		var isplaylist = $hdw('#videoplaylist').val(); 
		var title = $hdw('#videotitle').val();
		var type = $hdw('#videotype').val();
		var videoformat = [".flv", ".mp4", ".3g2", ".3gp", ".aac", ".f4b", ".f4p", ".f4v", ".m4a", ".m4v", ".mov", ".sdp", ".vp6"];
		var imageformat = ["jpg", "jpeg", "png", "gif"];
		var url_ = $hdw('#videourl').val();
		var hdurl_ = $hdw('#videohdurl').val();
		var thumb_ = $hdw('#videothumb').val();
		var preview_ = $hdw('#videopreview').val();
		var streamer_ = $hdw('#videostreamer').val();
		var token_ = $hdw('#videotoken').val();
		var dvr_ = ($hdw('#videodvr').is(':checked')) ? 1 : "" ;

		if($hdw('#videotype').val() == 'videoupload' && $hdw.trim($hdw('#videotitle').val()) != '')
		{
			if($hdw('#videourl').val() == '')
			{
				$hdw('#hdw_status').text("Make Sure You Uploaded File").css({"background":"#FFcABA","color":"#D8000C"}).show().fadeOut(3000);
				$hdw('body').scrollTop(0);
			}
			else if($hdw('#videoplaylist').val() == 'playlist')
			{
				$hdw('#hdw_status').text("Please Select Playlist").css({"background":"#FFcABA","color":"#D8000C"}).show().fadeOut(3000);
				$hdw('body').scrollTop(0);
			}
			else
			{
				inserturl(id, mytoken, title, isplaylist, url_, "", "video", thumb_, preview_, "", "", "" );
			}
		}
		else if($hdw.trim($hdw('#videotitle').val()) != "" && $hdw.trim($hdw('#videourl').val()) != "")
		{	
			if($hdw('#videoplaylist').val() == "playlist")
			{
				$hdw('#hdw_status').text("Please Select Playlist").css({"background":"#FFcABA","color":"#D8000C"}).show().fadeOut(3000);
				$hdw('body').scrollTop(0);
				return false;
			}
			
			var http = (url_.indexOf('http:') != -1) ? "true" : "false" ;
			var hdhttp = (hdurl_.indexOf('http:') != -1) ? "true" : "false" ;
			var thumbhttp = (thumb_.indexOf('http:') != -1) ? "true" : "false" ;
			var previewhttp = (preview_.indexOf('http:') != -1) ? "true" : "false" ;
			
			var urlext = url_.slice(-4);
			var hdurlext = hdurl_.slice(-4);
			
			urlext = ($hdw.inArray(urlext,videoformat) != -1) ? "true" : "false" ;
			hdurlext = ($hdw.inArray(hdurlext,videoformat) != -1) ? "true" : "false" ;
			
			var thumb = (thumb_.slice(-5)).indexOf(".");
			thumb = (thumb_.slice(-5)).substring(thumb + 1);
			
			var preview = (preview_.slice(-5)).indexOf(".");
			preview = (preview_.slice(-5)).substring(preview + 1);

			thumb = ($hdw.inArray(thumb,imageformat) != -1) ? "true" : "false" ;
			preview = ($hdw.inArray(preview,imageformat) != -1) ? "true" : "false" ;
			
			switch (type)
			{
				case 'youtube':
					url_ = (url_.indexOf('youtu.be') != -1) ? "http://www.youtube.com/watch?v=" + url_.slice(-11) : url_;
					var isyoutubeurl = /((http|https):\/\/)?(www\.)?(youtube\.com)(\/)?([a-zA-Z0-9\-\.]+)\/?/.test(url_);
					if(isyoutubeurl == true )
					{
						var youtubeid = url_.slice(-11);
						var youtubethumb = "http://img.youtube.com/vi/" + youtubeid + "/default.jpg";
						var youtubepreview = "http://img.youtube.com/vi/" + youtubeid + "/0.jpg";
						inserturl(id, mytoken, title, isplaylist, url_, "", type, youtubethumb, youtubepreview, "", "", "" );
					}
					else
					{
						$hdw('#hdw_status').text("Youtube Url Wrong").css({"background":"#FFcABA","color":"#D8000C"}).show().fadeOut(3000);
						$hdw('body').scrollTop(0);
					}
				break;

				case 'vimeo':
						var isvimeourl = /http:\/\/(www\.)?vimeo.com\/(\d+)($|\/)/;
						if(url_.match(isvimeourl))
						{
							inserturl(id, mytoken, title, isplaylist, url_, "", type, "", "", "", "", "" );							
						}
						else
						{
							$hdw('#hdw_status').text("Vimeo Url Wrong").css({"background":"#FFcABA","color":"#D8000C"}).show().fadeOut(3000);
							$hdw('body').scrollTop(0);
						}
					break;

				case 'dailymotion':
					var isdaily = /^.+dailymotion.com\/(video)\/([^_]+)[^#]*(#video=([^_&]+))?/;
					if(url_.match(isdaily))
					{
						var dailythumb = "http://www.dailymotion.com/thumbnail/video/" + url_.substring(33);
						inserturl(id, mytoken, title, isplaylist, url_, "", type, dailythumb, dailythumb, "", "", "" );
					}
					else
					{
						$hdw('#hdw_status').text("").show().text("Dailymotion Url Wrong").css({"background":"#FFcABA","color":"#D8000C"}).show().fadeOut(3000);
						$hdw('body').scrollTop(0);
					}
					break;
					
				case 'direct':
					if(hdurl_ == "" && thumb_ == "" && preview_ == "")
					{
						if(urlext == "true" && http == "true")
						{
							inserturl(id, mytoken, title, isplaylist, url_, "" , "video", "", "", "", "", "" );
						}
						else
						{
							$hdw('#hdw_status').text("Check Details").css({"background":"#FFcABA","color":"#D8000C"}).show().fadeOut(3000);
							$hdw('body').scrollTop(0);
						}
					}
					else if(hdurl_ == "" && thumb_ == "")
					{
						if(urlext == "true" && http == "true" && preview == "true" && previewhttp == "true")
						{
							inserturl(id, mytoken, title, isplaylist, url_, "" , "video", "", preview_, "", "", "" );
						}
						else
						{
							$hdw('#hdw_status').text("Check Details").css({"background":"#FFcABA","color":"#D8000C"}).show().fadeOut(3000);
							$hdw('body').scrollTop(0);
						}
					}
					else if(thumb_ == "" && preview_ == "")
					{
						if(urlext == "true" && http == "true" && hdurlext == "true" && hdhttp == "true")
						{
							inserturl(id, mytoken, title, isplaylist, url_, hdurl_ , "video", "", "", "", "", "" );
						}
						else
						{
							$hdw('#hdw_status').text("Check Details").css({"background":"#FFcABA","color":"#D8000C"}).show().fadeOut(3000);
							$hdw('body').scrollTop(0);
						}
					}
					else if(hdurl_ == "" && preview_ == "")
					{
						if(urlext == "true" && http == "true" && thumb == "true" && thumbhttp == "true")
						{
							inserturl(id, mytoken, title, isplaylist, url_, "" , "video", thumb_, "", "", "", "" );
						}
						else
						{
							$hdw('#hdw_status').text("Check Details").css({"background":"#FFcABA","color":"#D8000C"}).show().fadeOut(3000);
							$hdw('body').scrollTop(0);
						}
					}
					else if(hdurl_ == "")
					{
						if(urlext == "true" && http == "true" && thumb == "true" && thumbhttp == "true" && preview == "true" && previewhttp == "true")
						{
							inserturl(id, mytoken, title, isplaylist, url_, "", "video", thumb_, preview_, "", "", "" );
						}
						else
						{
							$hdw('#hdw_status').text("Check Details").css({"background":"#FFcABA","color":"#D8000C"}).show().fadeOut(3000);
							$hdw('body').scrollTop(0);
						}
					}
					else if(thumb_ == "")
					{
						if(urlext == "true" && http == "true" && hdurlext == "true" && hdhttp == "true" && preview == "true" && previewhttp == "true")
						{
							inserturl(id, mytoken, title, isplaylist, url_, hdurl_ , "video", "", preview_, "", "", "" );
						}
						else
						{
							$hdw('#hdw_status').text("Check Details").css({"background":"#FFcABA","color":"#D8000C"}).show().fadeOut(3000);
							$hdw('body').scrollTop(0);
						}
					}
					else if(preview_ == "")
					{
						if(urlext == "true" && http == "true" && hdurlext == "true" && hdhttp == "true" && thumb == "true" && thumbhttp == "true")
						{
							inserturl(id, mytoken, title, isplaylist, url_, hdurl_ , "video", thumb_, "", "", "", "" );
						}
						else
						{
							$hdw('#hdw_status').text("Check Details").css({"background":"#FFcABA","color":"#D8000C"}).show().fadeOut(3000);
							$hdw('body').scrollTop(0);
						}
					}
					else
					{
						inserturl(id, mytoken, title, isplaylist, url_, hdurl_, "video", thumb_, preview_, "", "", "" );
					}
				break;
	
				case 'rtmp':
					if (streamer_ != "")
					{
						if(thumb_ == "" && preview_ == "")
						{
							if(urlext == "true")
							{
								inserturl(id, mytoken, title, isplaylist, url_, "" , "rtmp", "", "", streamer_, token_, "" );
							}
							else
							{
								$hdw('#hdw_status').text("Check Details").css({"background":"#FFcABA","color":"#D8000C"}).show().fadeOut(3000);
								$hdw('body').scrollTop(0);
							}
						}
						else if(thumb_ == "")
						{
							if(urlext == "true" && preview == "true" && previewhttp == "true")
							{
								inserturl(id, mytoken, title, isplaylist, url_, "" , "rtmp", "", preview_, streamer_, token_, "" );
							}
							else
							{
								$hdw('#hdw_status').text("Check Details").css({"background":"#FFcABA","color":"#D8000C"}).show().fadeOut(3000);
								$hdw('body').scrollTop(0);
							}
						}
						else if(preview_ == "")
						{
							if(urlext == "true" && thumb == "true" && thumbhttp == "true")
							{
								inserturl(id, mytoken, title, isplaylist, url_, "" , "rtmp", thumb_, "", streamer_, token_, "" );
							}
							else
							{
								$hdw('#hdw_status').text("Check Details").css({"background":"#FFcABA","color":"#D8000C"}).show().fadeOut(3000);
								$hdw('body').scrollTop(0);
							}
						}
						else
						{
							inserturl(id, mytoken, title, isplaylist, url_, "" , "rtmp", thumb_, preview_, streamer_, token_, "" );
						}
					}
					else
					{
						$hdw('#hdw_status').text("Streamer Should Not Be Empty").css({"background":"#FFcABA","color":"#D8000C"}).show().fadeOut(3000);
						$hdw('body').scrollTop(0);
					}
					break;
	
				case 'smil':
					urlext = url_.slice(-5);
					if(thumb_ == "" && preview_ == "")
					{
						if(urlext == ".smil" && http == "true")
						{
							inserturl(id, mytoken, title, isplaylist, url_, "" , "highwinds", "", "", "", "", "" );
						}
						else
						{
							$hdw('#hdw_status').text("Check Details").css({"background":"#FFcABA","color":"#D8000C"}).show().fadeOut(3000);
							$hdw('body').scrollTop(0);
						}
					}
					else if(thumb_ == "")
					{
						if(urlext == ".smil" && http == "true" && preview == "true" && previewhttp == "true")
						{
							inserturl(id, mytoken, title, isplaylist, url_, "" , "highwinds", "", preview_, "", "", "" );
						}
						else
						{
							$hdw('#hdw_status').text("Check Details").css({"background":"#FFcABA","color":"#D8000C"}).show().fadeOut(3000);
							$hdw('body').scrollTop(0);
						}
					}
					else if(preview_ == "")
					{
						if(urlext == ".smil" && http == "true" && thumb == "true" && thumbhttp == "true")
						{
							inserturl(id, mytoken, title, isplaylist, url_, "" , "highwinds", thumb_, "", "", "", "" );
						}
						else
						{
							$hdw('#hdw_status').text("Check Details").css({"background":"#FFcABA","color":"#D8000C"}).show().fadeOut(3000);
							$hdw('body').scrollTop(0);
						}
					}
					else
					{
						inserturl(id, mytoken, title, isplaylist, url_, "" , "highwinds", thumb_, preview_, "", "", "" );
					}
				break;
	
				case 'lighttpd':
					if(hdurl_ == "" && thumb_ == "" && preview_ == "")
					{
						if(urlext == "true" && http == "true")
						{
							inserturl(id, mytoken, title, isplaylist, url_, "" , "lighttpd", "", "", "", "", "" );
						}
						else
						{
							$hdw('#hdw_status').text("Check Details").css({"background":"#FFcABA","color":"#D8000C"}).show().fadeOut(3000);
							$hdw('body').scrollTop(0);
						}
					}
					else if(hdurl_ == "" && thumb_ == "")
					{
						if(urlext == "true" && http == "true" && preview == "true" && previewhttp == "true")
						{
							inserturl(id, mytoken, title, isplaylist, url_, "" , "lighttpd", "", preview_, "", "", "" );
						}
						else
						{
							$hdw('#hdw_status').text("Check Details").css({"background":"#FFcABA","color":"#D8000C"}).show().fadeOut(3000);
							$hdw('body').scrollTop(0);
						}
					}
					else if(thumb_ == "" && preview_ == "")
					{
						if(urlext == "true" && http == "true" && hdurlext == "true" && hdhttp == "true")
						{
							inserturl(id, mytoken, title, isplaylist, url_, hdurl_ , "lighttpd", "", "", "", "", "" );
						}
						else
						{
							$hdw('#hdw_status').text("Check Details").css({"background":"#FFcABA","color":"#D8000C"}).show().fadeOut(3000);
							$hdw('body').scrollTop(0);
						}
					}
					else if(hdurl_ == "" && preview_ == "")
					{
						if(urlext == "true" && http == "true" && thumb == "true" && thumbhttp == "true")
						{
							inserturl(id, mytoken, title, isplaylist, url_, "" , "lighttpd", thumb_, "", "", "", "" );
						}
						else
						{
							$hdw('#hdw_status').text("Check Details").css({"background":"#FFcABA","color":"#D8000C"}).show().fadeOut(3000);
							$hdw('body').scrollTop(0);
						}
					}
					else if(hdurl_ == "")
					{
						if(urlext == "true" && http == "true" && thumb == "true" && thumbhttp == "true" && preview == "true" && previewhttp == "true")
						{
							inserturl(id, mytoken, title, isplaylist, url_, "", "lighttpd", thumb_, preview_, "", "", "" );
						}
						else
						{
							$hdw('#hdw_status').text("Check Details").css({"background":"#FFcABA","color":"#D8000C"}).show().fadeOut(3000);
							$hdw('body').scrollTop(0);
						}
					}
					else if(thumb_ == "")
					{
						if(urlext == "true" && http == "true" && hdurlext == "true" && hdhttp == "true" && preview == "true" && previewhttp == "true")
						{
							inserturl(id, mytoken, title, isplaylist, url_, hdurl_ , "lighttpd", "", preview_, "", "", "" );
						}
						else
						{
							$hdw('#hdw_status').text("Check Details").css({"background":"#FFcABA","color":"#D8000C"}).show().fadeOut(3000);
							$hdw('body').scrollTop(0);
						}
					}
					else if(preview_ == "")
					{
						if(urlext == "true" && http == "true" && hdurlext == "true" && hdhttp == "true" && thumb == "true" && thumbhttp == "true")
						{
							inserturl(id, mytoken, title, isplaylist, url_, hdurl_ , "lighttpd", thumb_, "", "", "", "" );
						}
						else
						{
							$hdw('#hdw_status').text("Check Details").css({"background":"#FFcABA","color":"#D8000C"}).show().fadeOut(3000);
							$hdw('body').scrollTop(0);
						}
					}
					else
					{
						inserturl(id, mytoken, title, isplaylist, url_, hdurl_, "lighttpd", thumb_, preview_, "", "", "" );
					}
				break;
	
				case 'bitgravity':
					if(hdurl_ == "" && thumb_ == "" && preview_ == "")
					{
						if(urlext == "true" && http == "true")
						{
							inserturl(id, mytoken, title, isplaylist, url_, "" , "bitgravity", "", "", "", "", dvr_ );
						}
						else
						{
							$hdw('#hdw_status').text("Check Details").css({"background":"#FFcABA","color":"#D8000C"}).show().fadeOut(3000);
							$hdw('body').scrollTop(0);
						}
					}
					else if(hdurl_ == "" && thumb_ == "")
					{
						if(urlext == "true" && http == "true" && preview == "true" && previewhttp == "true")
						{
							inserturl(id, mytoken, title, isplaylist, url_, "" , "bitgravity", "", preview_, "", "", dvr_ );
						}
						else
						{
							$hdw('#hdw_status').text("Check Details").css({"background":"#FFcABA","color":"#D8000C"}).show().fadeOut(3000);
							$hdw('body').scrollTop(0);
						}
					}
					else if(thumb_ == "" && preview_ == "")
					{
						if(urlext == "true" && http == "true" && hdurlext == "true" && hdhttp == "true")
						{
							inserturl(id, mytoken, title, isplaylist, url_, hdurl_ , "bitgravity", "", "", "", "", dvr_ );
						}
						else
						{
							$hdw('#hdw_status').text("Check Details").css({"background":"#FFcABA","color":"#D8000C"}).show().fadeOut(3000);
							$hdw('body').scrollTop(0);
						}
					}
					else if(hdurl_ == "" && preview_ == "")
					{
						if(urlext == "true" && http == "true" && thumb == "true" && thumbhttp == "true")
						{
							inserturl(id, mytoken, title, isplaylist, url_, "" , "bitgravity", thumb_, "", "", "", dvr_ );
						}
						else
						{
							$hdw('#hdw_status').text("Check Details").css({"background":"#FFcABA","color":"#D8000C"}).show().fadeOut(3000);
							$hdw('body').scrollTop(0);
						}
					}
					else if(hdurl_ == "")
					{
						if(urlext == "true" && http == "true" && thumb == "true" && thumbhttp == "true" && preview == "true" && previewhttp == "true")
						{
							inserturl(id, mytoken, title, isplaylist, url_, "", "bitgravity", thumb_, preview_, "", "", dvr_ );
						}
						else
						{
							$hdw('#hdw_status').text("Check Details").css({"background":"#FFcABA","color":"#D8000C"}).show().fadeOut(3000);
							$hdw('body').scrollTop(0);
						}
					}
					else if(thumb_ == "")
					{
						if(urlext == "true" && http == "true" && hdurlext == "true" && hdhttp == "true" && preview == "true" && previewhttp == "true")
						{
							inserturl(id, mytoken, title, isplaylist, url_, hdurl_ , "bitgravity", "", preview_, "", "", dvr_ );
						}
						else
						{
							$hdw('#hdw_status').text("Check Details").css({"background":"#FFcABA","color":"#D8000C"}).show().fadeOut(3000);
							$hdw('body').scrollTop(0);
						}
					}
					else if(preview_ == "")
					{
						if(urlext == "true" && http == "true" && hdurlext == "true" && hdhttp == "true" && thumb == "true" && thumbhttp == "true")
						{
							inserturl(id, mytoken, title, isplaylist, url_, hdurl_ , "bitgravity", thumb_, "", "", "", dvr_ );
						}
						else
						{
							$hdw('#hdw_status').text("Check Details").css({"background":"#FFcABA","color":"#D8000C"}).show().fadeOut(3000);
							$hdw('body').scrollTop(0);
						}
					}
					else
					{
						inserturl(id, mytoken, title, isplaylist, url_, hdurl_, "bitgravity", thumb_, preview_, "", "", dvr_ );
					}
				break;
			}
		}
		else
		{
			$hdw('#hdw_status').text("Title and Url Should Not Be Empty").css({"background":"#FFcABA","color":"#D8000C"}).show().fadeOut(3000);
			$hdw('body').scrollTop(0);
		}
	});	
	
	$hdw('#videostreamer, #videotoken, #videodvr').attr("disabled","disabled").css("opacity","0.4");
	
	$hdw('#videotype').change(function (){
		
		switch ($hdw('#videotype').val())
		{
		case 'direct':
			$hdw('.hdwupload, #videostreamer, #videotoken, #videodvr').attr("disabled","disabled").css("opacity","0.4");
			$hdw('#videohdurl, #videopreview, #videothumb').attr("disabled",false).css("opacity","1");
			break;

		case 'videoupload':
			$hdw('#videostreamer, #videotoken, #videodvr, #videohdurl').attr("disabled","disabled").css("opacity","0.4");
			$hdw('.hdwupload, #videopreview, #videothumb').attr("disabled",false).css("opacity","1");
			break;

		case 'youtube':
			$hdw('.hdwupload, #videostreamer, #videotoken, #videodvr, #videohdurl, #videopreview, #videothumb').attr("disabled","disabled").css("opacity","0.4");
			break;

		case 'vimeo':
			$hdw('.hdwupload, #videostreamer, #videotoken, #videodvr, #videohdurl, #videopreview, #videothumb').attr("disabled","disabled").css("opacity","0.4");
			break;

		case 'dailymotion':
			$hdw('.hdwupload, #videostreamer, #videotoken, #videodvr, #videohdurl, #videopreview, #videothumb').attr("disabled","disabled").css("opacity","0.4");
			break;

		case 'rtmp':
			$hdw('.hdwupload, #videodvr, #videohdurl').attr("disabled","disabled").css("opacity","0.4");
			$hdw('#videostreamer, #videotoken, #videopreview, #videothumb').attr("disabled",false).css("opacity","1");
			break;

		case 'smil':
			$hdw('.hdwupload, #videostreamer, #videotoken, #videodvr, #videohdurl').attr("disabled","disabled").css("opacity","0.4");
			$hdw('#videopreview, #videothumb').attr("disabled",false).css("opacity","1");
			break;

		case 'lighttpd':
			$hdw('.hdwupload, #videostreamer, #videotoken, #videodvr').attr("disabled","disabled").css("opacity","0.4");
			$hdw('#videopreview, #videothumb, #videohdurl').attr("disabled",false).css("opacity","1");
			break;

		case 'bitgravity':
			$hdw('.hdwupload, #videostreamer, #videotoken').attr("disabled","disabled").css("opacity","0.4");
			$hdw('#videopreview, #videothumb, #videodvr').attr("disabled",false).css("opacity","1");
			break;
		}
	});
	
	$hdw('#videourl').change(function (){
		url_ = $hdw('#videourl').val();
		url_ = (url_.indexOf('youtu.be') != -1) ? "http://www.youtube.com/watch?v=" + url_.slice(-11) : url_;
		var isyoutubeurl = /((http|https):\/\/)?(www\.)?(youtube\.com)(\/)?([a-zA-Z0-9\-\.]+)\/?/.test(url_);
		var isdaily = /^.+dailymotion.com\/(video)\/([^_]+)[^#]*(#video=([^_&]+))?/;
		if(isyoutubeurl == true )
		{
			var youtubeid = url_.slice(-11);
			$hdw('#videothumb').val("http://img.youtube.com/vi/" + youtubeid + "/default.jpg").attr('disabled',true);
			$hdw('#videopreview').val("http://img.youtube.com/vi/" + youtubeid + "/0.jpg").attr('disabled',true);
		}
		else if(url_.match(isdaily))
		{
			$hdw('#videothumb, #videopreview').val("http://www.dailymotion.com/thumbnail/video/" + url_.substring(33)).attr('disabled',true);			
		}
		else
		{
			$hdw('#videothumb').val("").attr('disabled',false);
			$hdw('#videopreview').val("").attr('disabled',false);
		}
	});
	
	$hdw('#width, #height, #buffer, #volumelevel').bind('change keyup', function () {
		var currentid = this.id;
		var currentvalue = $hdw('#' + currentid).val();	

		switch (currentid)
		{
			case 'width':
				currentvalue = parseInt((currentvalue == "") ? 100 : currentvalue);
				//currentvalue = parseInt((currentvalue <= 200) ? 200 : currentvalue);
				currentvalue = parseInt((currentvalue >= 1366) ? 1366 : currentvalue);
				$hdw('#width').val(currentvalue);
				break;
	
			case 'height':
				currentvalue = parseInt((currentvalue == "") ? 100 : currentvalue);
				//currentvalue = parseInt((currentvalue <= 200) ? 200 : currentvalue);
				currentvalue = parseInt((currentvalue >= 640) ? 640 : currentvalue);
				$hdw('#height').val(currentvalue);
				break;
	
			case 'buffer':
				currentvalue = parseInt((currentvalue == "") ? 0 : currentvalue);
				currentvalue = parseInt((currentvalue >= 10) ? 10 : currentvalue);
				$hdw('#buffer').val(currentvalue);
				break;
	
			case 'volumelevel':
				currentvalue = parseInt((currentvalue == "") ? 0 : currentvalue);
				currentvalue = parseInt((currentvalue >= 100) ? 100 : currentvalue);
				$hdw('#volumelevel').val(currentvalue);
				break;
		}
	});

	$hdw('#radiosingle').click(function (){
		$hdw('#radiosingle').attr('checked',true);
		$hdw('#radioplaylist').attr('checked',false);
		$hdw('#singlevideoid').attr('disabled',false);
		$hdw('#playerplaylist, #playlistopen, #playlistautoplay, #galleryoption, #playergallery').attr('disabled',true);
	});
	
	$hdw('#radioplaylist').click(function (){
		$hdw('#radioplaylist').attr('checked',true);
		$hdw('#radiosingle').attr('checked',false);
		$hdw('#playerplaylist, #playlistopen, #playlistautoplay, #galleryoption, #playergallery').attr('disabled',false);
		$hdw('#singlevideoid').attr('disabled',true);
		
	});
	
	//Hover Effects
	$hdw('#videototalcontent, #playlisttotalcontent, #playertotalcontent, #gallerytotalcontent')
	
		.delegate('table td :button','mouseover',function (){
			if(this.id != 'deletevideos')
			{
				$hdw(this).css("color","red");
			}
		})	
		.delegate('table td :button','mouseout',function (){
			if(this.id != 'deletevideos')
			{
				$hdw(this).css("color","black");
			}
		});
	
	$hdw('#playerlists').bind('click hover',function (){
		$hdw('.color').each(function (){
			var rgb = $hdw(this).css('background-color');
		    rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
		    function hex(x) {return ("0" + parseInt(x).toString(16)).slice(-2);}
		    rgb = '#' + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
		    $hdw(this).css('color', rgb);
		});
	});
	//------------------------------------------------------
	
	function saveplayer(gallerydisplay, gallery, id, width, height, buffer, volumelevel, skinlist, videoplaytype, videoid, playlistname)
	{
		var playerid = $hdw('#width').attr('class');
		$hdw.ajax({
			
			type: 'post',
			url:Drupal.settings.basePath + "?q=playerdetails",
		    data:
		    	{
					gallerydisplay:gallerydisplay, 
					gallery:gallery,
		    		playerid:playerid,
					action:id,
					token:mytoken,
					width:width,
					height:height,
					buffer:buffer,
					volumelevel:volumelevel,
					skinlist:skinlist,
					videoid:videoid,
					playlistname:playlistname,
					videoplaytype:videoplaytype
				},
			dataType: "json",
			success: function(data){				
				
				if (id == 'updateplayerbutton')
				{
					var index;
					var row = $hdw('#width').attr('class');
					for(i=0;i<data.length;i++)
					{
						if(row == data[i].playerid) { index = i; }
					}
					var vid = (data[index].vid == 0) ? "-" : data[index].vid;
		    		var playlist = (data[index].playlist == "") ? "-" : data[index].playlist;
		    		var players = '<tr id="pl' + data[index].playerid + '"><td id="player' + data[index].playerid + '">' + data[index].playerid + '</td>';
		    		players += '<td id="video' + data[index].playerid + '">' + vid + '</td>';
		    		players += '<td id="plist' + data[index].playerid + '">' + playlist + '</td>';
		    		players += '<td id="width' + data[index].playerid + '">' + data[index].width + '</td>';
		    		players += '<td id="height' + data[index].playerid + '">' + data[index].height + '</td>';
		    		players += '<td><input id="updateplayer' + data[index].playerid + '" type="button" value="Udpate" style="cursor:pointer; border:1px solid #ccc; height:20px; width:80px;"/></td>';
		    		players += '<td><input id="deleteplayer' + data[index].playerid + '" type="button" value="Delete" style="cursor:pointer; border:1px solid #ccc; height:20px; width:80px;"/></td>';
		    		players += '</tr>';
					$hdw('#pl' + row).replaceWith(players);
					$hdw('#width').attr('class','');
					$hdw('#hdw_status').text("Player Updated Successfully").css({"background":"#DFF2BF","color":"#4F8A10"}).show().fadeOut(3000);
				}
				else if(id == 'saveplayer')
				{		    		
					var index = data.length - 1;
					var vid = (data[index].vid == 0) ? "-" : data[index].vid;
		    		var playlist = (data[index].playlist == "") ? "-" : data[index].playlist;
		    		var players = '<tr id="pl' + data[index].playerid + '"><td id="player' + data[index].playerid + '">' + data[index].playerid + '</td>';
		    		players += '<td id="video' + data[index].playerid + '">' + vid + '</td>';
		    		players += '<td id="plist' + data[index].playerid + '">' + playlist + '</td>';
		    		players += '<td id="width' + data[index].playerid + '">' + data[index].width + '</td>';
		    		players += '<td id="height' + data[index].playerid + '">' + data[index].height + '</td>';
		    		players += '<td><input id="updateplayer' + data[index].playerid + '" type="button" value="Udpate" style="cursor:pointer; border:1px solid #ccc; height:20px; width:80px;"/></td>';
		    		players += '<td><input id="deleteplayer' + data[index].playerid + '" type="button" value="Delete" style="cursor:pointer; border:1px solid #ccc; height:20px; width:80px;"/></td>';
		    		players += '</tr>';

		    		$hdw('#playertotalcontent table').append(players);
		    		$hdw('#hdw_status').text("Player Created Successfully").css({"background":"#DFF2BF","color":"#4F8A10"}).show().fadeOut(3000);
				}	
				
				$hdw('#playerlists, #playertotalcontent').show();
				$hdw('#emptyplayer, #insertvideo, #insertplaylist, #documentationdiv, #videototalcontent, #playlisttotalcontent, #updateplaylistcontent, #saveplayer, #cancelplayer, #playerdetails, #updateplayerbutton, #cancelplayer_update').hide();
			}			
		});
	}
	
	$hdw('#addgallery').click(function (){
		$hdw('#gallerytotalcontent, #updategallerys').hide();
		$hdw('#gallerydetails, #savegallerys').show();
		$hdw('#galleryname').attr("class","");
		$hdw('#gallerydetails :text').val("");
	});
	
	$hdw('#cancelgallery, #cancelgallery_update').click(function (){
		$hdw('#gallerydetails').hide();
		$hdw('#gallerytotalcontent').show();
	});
	
	function savegallery(id, name, rows, columns, displaylimit, thumbwidth, thumbheight, thumbspace)
	{
		var names = $hdw('#galleryname').attr("class");
		if(names != "") { names = names.split('`'); }else { names = ['', '']; }
		if(name != names[1] || rows != $hdw('#galleryrows').attr("class") || columns != $hdw('#gallerycolumns').attr("class") || displaylimit != $hdw('#gallerylimit').attr("class") || thumbwidth != $hdw('#gallerywidth').attr("class") || thumbheight != $hdw('#galleryheight').attr("class") || thumbspace != $hdw('#galleryspace').attr("class"))
		{
			$hdw.ajax({
				
				type: 'post',
				url:Drupal.settings.basePath + "?q=hdwgallerysettings",
			    data:
			    	{
						oldname:names[1],
						updategalleryid:names[0],
						actionid:id,
						name:name,
						rows:rows,
						columns:columns,
						limit:displaylimit,
						thumbwidth:thumbwidth,
						thumbheight:thumbheight,
						thumbspace:thumbspace,
						drupaltoken:mytoken
					},
				dataType: "json",
				success: function(data){

					if(data == "Already exists")
					{
						$hdw('#hdw_status').text($hdw('#galleryname').val() + " is already exist.").css({"background":"#FFcABA","color":"#D8000C"}).show().fadeOut(3000);
						$hdw('#galleryname').focus();
					}
					else if(id == 'savegallery')
					{
						var lastindex = data.length - 1;
						$hdw('#playergallery').append('<option value="' + data[lastindex].name + '">' + data[lastindex].name + '</option>');
						$hdw('#gallerydetails :text').val("");
						
						var gallery = '<tr id="gl' + data[lastindex].gid + '"><td id="gallery' + data[lastindex].gid + '">' + data[lastindex].gid + '</td>';
						gallery += '<td id="glname' + data[lastindex].gid + '">' + data[lastindex].name + '</td>';
						gallery += '<td id="glrows' + data[lastindex].gid + '">' + data[lastindex].rows + '</td>';
			    		gallery += '<td id="glcolumns' + data[lastindex].gid + '">' + data[lastindex].columns + '</td>';
			    		gallery += '<td id="gllimit' + data[lastindex].gid + '">' + data[lastindex].limits + '</td>';
			    		gallery += '<td id="glwidth' + data[lastindex].gid + '">' + data[lastindex].thumbwidth + '</td>';
			    		gallery += '<td id="glheight' + data[lastindex].gid + '">' + data[lastindex].thumbheight + '</td>';
			    		gallery += '<td id="glspace' + data[lastindex].gid + '">' + data[lastindex].thumbspace + '</td>';
			    		gallery += '<td><input id="updategallery' + data[lastindex].gid + '" type="button" value="Udpate" style="cursor:pointer; border:1px solid #ccc; height:20px; width:80px;"/></td>';
			    		gallery += '<td><input id="deletegallery' + data[lastindex].gid + '" type="button" value="Delete" style="cursor:pointer; border:1px solid #ccc; height:20px; width:80px;"/></td>';
			    		gallery += '</tr>';
						
				    	$hdw('#gallerytotalcontent table').append(gallery);
						$hdw('#gallerydetails, #emptygallery').hide();
						$hdw('#gallerytotalcontent').show();
						$hdw('#hdw_status').text("Gallery Created Successfully").css({"background":"#DFF2BF","color":"#4F8A10"}).show().fadeOut(3000);
						$hdw('body').scrollTop(0);
					}
					else if(id == 'updategallerybutton')
					{
						var lastindex; 
						var row = $hdw('#galleryname').attr('class');
						row = row.split('`');
						for(i=0;i<data.length;i++)
						{
							if(row[0] == data[i].gid) { lastindex = i; }
						}
						var gallery = '<tr id="gl' + data[lastindex].gid + '"><td id="gallery' + data[lastindex].gid + '">' + data[lastindex].gid + '</td>';
						gallery += '<td id="glname' + data[lastindex].gid + '">' + data[lastindex].name + '</td>';
						gallery += '<td id="glrows' + data[lastindex].gid + '">' + data[lastindex].rows + '</td>';
			    		gallery += '<td id="glcolumns' + data[lastindex].gid + '">' + data[lastindex].columns + '</td>';
			    		gallery += '<td id="gllimit' + data[lastindex].gid + '">' + data[lastindex].limits + '</td>';
			    		gallery += '<td id="glwidth' + data[lastindex].gid + '">' + data[lastindex].thumbwidth + '</td>';
			    		gallery += '<td id="glheight' + data[lastindex].gid + '">' + data[lastindex].thumbheight + '</td>';
			    		gallery += '<td id="glspace' + data[lastindex].gid + '">' + data[lastindex].thumbspace + '</td>';
			    		gallery += '<td><input id="updategallery' + data[lastindex].gid + '" type="button" value="Udpate" style="cursor:pointer; border:1px solid #ccc; height:20px; width:80px;"/></td>';
			    		gallery += '<td><input id="deletegallery' + data[lastindex].gid + '" type="button" value="Delete" style="cursor:pointer; border:1px solid #ccc; height:20px; width:80px;"/></td>';
			    		gallery += '</tr>';
						
						$hdw('#gl' + row).replaceWith(gallery);
						$hdw('#gallerydetails, #emptygallery').hide();
						$hdw('#gallerytotalcontent').show();
						$hdw('#galleryname').attr('class','');
						$hdw('#hdw_status').text("Gallery Updated Successfully").css({"background":"#DFF2BF","color":"#4F8A10"}).show().fadeOut(3000);
						$hdw('body').scrollTop(0);
						
						$hdw('#playergallery option').each(function(){
						    if (this.value == names[1]) {
						    	$hdw(this).replaceWith("<option value='" + data[lastindex].name + "'>" + data[lastindex].name + "</option>");
						    }
						});
					}
				}					
			});
		}
		else
		{
			$hdw('#gallerydetails, #updategallerys').hide();
			$hdw('#gallerytotalcontent').show();
			$hdw('#hdw_status').text("No changes have been made.").css({"background":"#DFF2BF","color":"#4F8A10"}).show().fadeOut(3000);
			$hdw('#galleryname').focus();
		}
	}
	
	$hdw('#savegallery, #updategallerybutton').click(function (){
		
		var id = this.id;
		var name			= $hdw.trim($hdw('#galleryname').val());
		var rows 			= ($hdw('#galleryrows').val()).match(/\d+/) == null 	? 2 	: $hdw('#galleryrows').val().match(/\d+/) ;
		var columns			= ($hdw('#gallerycolumns').val()).match(/\d+/) == null 	? 3 	: $hdw('#gallerycolumns').val().match(/\d+/) ;
		var displaylimit 	= ($hdw('#gallerylimit').val()).match(/\d+/) == null 	? 6 	: $hdw('#gallerylimit').val().match(/\d+/) ;
		var thumbwidth 		= ($hdw('#gallerywidth').val()).match(/\d+/) == null 	? 100 	: $hdw('#gallerywidth').val().match(/\d+/) ;
		var thumbheight 	= ($hdw('#galleryheight').val()).match(/\d+/) == null 	? 70 	: $hdw('#galleryheight').val().match(/\d+/) ;
		var thumbspace 		= ($hdw('#galleryspace').val()).match(/\d+/) == null 	? 2 	: $hdw('#galleryspace').val().match(/\d+/) ;
		if(name != "")
		{
			savegallery(id, name, rows, columns, displaylimit, thumbwidth, thumbheight, thumbspace);
		}
		else
		{
			$hdw('#hdw_status').text("Gallery Name is Mandatory").css({"background":"#FFcABA","color":"#D8000C"}).show().fadeOut(3000);
			$hdw('body').scrollTop(0);
		}
	});
	


	$hdw('#gallerytotalcontent').delegate('table td :button','click',function (){
		
		if(this.id.substr(0,5) == "delet")
		{
			var confirmmsg = confirm("Do you really want to delete this gallery?");
		    if (confirmmsg == true) 
		    {
				var action = this.id.substr(13);
				var gallery = $hdw('#glname' + action).text();
				$hdw('#gl' + action).remove();
				$hdw.ajax({				
					
					url:Drupal.settings.basePath + "?q=hdwgallerysettings",
					data:
						{
							drupaltoken:mytoken,
							galleryid:action
						},
					dataType: "json",
					success: function (data){
						if(data.length == 0) { $hdw('#emptygallery').show(); }
					}
				});
				
				$hdw('#playergallery option').each(function(){
				    if (this.value == gallery) {
				    	$hdw(this).remove();
				    }
				});
		    }
		}
		else if(this.id.substr(0,5) == "updat")
		{
			var action = this.id.substr(13);
			
			$hdw('#galleryname').val($hdw('#glname' + action).text()).attr("class", action + '`' + $hdw('#glname' + action).text());
			$hdw('#galleryrows').val($hdw('#glrows' + action).text()).attr("class", $hdw('#glrows' + action).text());
			$hdw('#gallerycolumns').val($hdw('#glcolumns' + action).text()).attr("class", $hdw('#glcolumns' + action).text());
			$hdw('#gallerylimit').val($hdw('#gllimit' + action).text()).attr("class", $hdw('#gllimit' + action).text());
			$hdw('#gallerywidth').val($hdw('#glwidth' + action).text()).attr("class", $hdw('#glwidth' + action).text());
			$hdw('#galleryheight').val($hdw('#glheight' + action).text()).attr("class", $hdw('#glheight' + action).text());
			$hdw('#galleryspace').val($hdw('#glspace' + action).text()).attr("class", $hdw('#glspace' + action).text());

			$hdw('#gallerytotalcontent, #savegallerys').hide();
			$hdw('#gallerydetails, #updategallerys').show();
			$hdw('#galleryname').focus();
		}
	});
	
});