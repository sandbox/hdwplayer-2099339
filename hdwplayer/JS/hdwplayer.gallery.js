jQuery(document).ready(function (){
	$hd = jQuery.noConflict();

	$hd.ajax({
		
		url:Drupal.settings.basePath + "?q=totalplayers",
	    data:
	    	{
				totalpl:'hdwplayer'
			},
	    dataType: "json",
		success: function(data){
			if(data.length != 0)
			{
				for(i=0;i<=data.length;i++)
				{
					$hd('#slider' + data[i].playerid).hdwplayers();
				}
			}
		}
		
	});
	
	$hd('.hdwplayer_gallery div ul li div div a, .hdwplayer_gallery div ul li div div img').click(function (){
		
		var sizes = ($hd(this).attr('class')).split('`');
		var flashvar = '';		
		
		flashvar += 'id=' + sizes[2] + '&vid=' + this.id + '&site=' + location.href;
		flashvar += '&baseD=http://' +  $hd(location).attr("host") + Drupal.settings.basePath;

		var player = '';
		player += '<param name="movie" value="http://' +  $hd(location).attr("host") + Drupal.settings.basePath + 'sites/all/modules/hdwplayer/player.swf" />';
		player += '<param name="wmode" value="opaque" />';
		player += '<param name="bgcolor" value="#000000" />';
		player += '<param name="allowfullscreen" value="true" />';
		player += '<param name="allowscriptaccess" value="always" />';
		player += '<param id="firstobject" name="flashvars" value="' + flashvar + '" />';
		player += '<object id="innerplayer" type="application/x-shockwave-flash" data="http://' +  $hd(location).attr("host") + Drupal.settings.basePath + 'sites/all/modules/hdwplayer/player.swf" width="' + sizes[0] + '" height="' + sizes[1] + '">';
		player += '<param name="movie" value="http://' +  $hd(location).attr("host") + Drupal.settings.basePath + 'sites/all/modules/hdwplayer/player.swf" />';
		player += '<param name="wmode" value="opaque" />';
		player += '<param name="bgcolor" value="#000000" />';
		player += '<param name="allowfullscreen" value="true" />';
		player += '<param name="allowscriptaccess" value="always" />';
		player += '<param id="secondobject" name="flashvars" value="' + flashvar + '"/>';
		player += '</object>';
			
		$hd('#hdwplayer' + sizes[2]).html(player);
	});
});