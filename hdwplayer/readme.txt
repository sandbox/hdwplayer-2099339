HDW Player Documentation

Features

1. Support flv, mp4, 3g2, 3gp, aac, f4b, f4p, f4v, m4a, m4v, mov(h.264), sdp, vp6.
2. HTML5 / Iphone / IPad / Android Support.
3. Youtube / Vimeo / Dailmotion Support.
4. Live Streaming / RTMP / Pseudo / CDN Support.
5. Javascript API Support.
6. XML Playlist Support.
7. Google Adsense Support.
8. Full Control on your Player.
9. Embed Options Support.
10. Social Media Sharing.
11. Email Sharing Support.
12. Tool Tip Support .
13. Custom Skin Color Support.

Player Setup

Creating Playlist:
Click Add Playlist Button.
Enter the Playlist name and Save.

Creating Video:
Click Add Video Button.
Enter the Video details, select playlist and Save.

Creating Gallery:
Click Add Gallery Button.
Enter the Gallery details and Save.

Creating Player:
Click Add Player Button.
Enter the Player details and Save.

Adding Playlist to Player:
Click Add Player Button.
Select Playlist radio button under Video Settings.
Select the Playlist and Save.

Adding Video to Player:
Click Add Player Button.
Select Video radio button under Video Settings.
Select Video Id and Save.

Adding Gallery to Player:
Click Add Player Button.
Select Playlist under Video Settings tab.
Set the Display Gallery from None to Display.
Select the Gallery and Save.

Displaying the Player to Front-end:
Select the Region for HDW Player Block from here and Save.
Or
Click the Structure button in drupal admin menubar.
Click Blocks and set the Region to HDW Player and Save.

Setting Permission To Particular Block (Optional)
Select the respective block from here and Save.
Click the Roles tab under Visibility Settings.
Set the roles as you wish and Save.