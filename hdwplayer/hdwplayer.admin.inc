<?php
/**
 * Administration settings for the HDW Player Drupal module.
 */
function hdwplayer_admin_settings() {
	
	$form = array();
	  
	global $user;
	$newToken = drupal_get_token("my secret value" . $user->uid);
	
	drupal_add_js("var mytoken = '$newToken';", "inline");
	
	$form['#suffix'] = '<div id="hdw_status"></div><br/>
	<table id="settingstable">
		<tr id="settingsheadingrow">
			<td id="playersettings" >Player</td>
			<td id="videosettings" >Video</td>
			<td id="playlistsettings" >Playlist</td>
			<td id="gallerysettings" >Gallery</td>
			<td id="documentation" >Documentation</td>
		</tr>
		<tr id="settingsbodyrow">
		<td id="settingsbody" colspan="5">
			<div id="playerlists">
				<input id="addplayer" type="button" value="Add Player"/><br/>
				<table id="playerdetails" >
				<tr>
				<td width="50%" style="vertical-align:top;">
					<table id="hdw_config">
					<tr><th colspan="2">Config Settings</th></tr>
					<tr><td>Width</td>	<td><input id="width" type="text" value="400" /></td></tr>
					<tr><td>Height</td>	<td><input id="height" type="text" value="250" /></td></tr>
					<tr><td>Buffer</td>	<td><input id="buffer" type="text" value="3" /></td></tr>
					<tr><td>Volume Level</td>	<td><input id="volumelevel" type="text" value="50" /></td></tr>
					<tr><td>SkinMode</td>	<td>
					<select id="skinmode" class="skinlist">
						<option value="static" selected="selected">static</option>
						<option value="float">float</option>
					</select>
					</td></tr>
					<tr><td>Stretch</td>	<td>
					<select id="stretch" class="skinlist">
						<option value="fill" selected="selected">fill</option>
						<option value="uniform">uniform</option>
						<option value="exactfit">exactfit</option>
						<option value="none">none</option>
					</select>
					</td></tr>
					<tr>
					<td colspan="2"><strong>Video Settings</strong></td>
					</tr>					
					<tr><td>
					<input id="radiosingle" type="radio" value="single" /> Video</td><td>
					<input id="radioplaylist" type="radio" value="playlist" checked="checked" /> Playlist</td></tr>
					<tr>
					<td>Video Id</td><td>
					<select id="singlevideoid" disabled="disabled">
						<option value="video" selected="selected">Select</option>
					</select>
					</td></tr>
					<tr>
					<tr><td>Playlist</td><td>
					<select id="playerplaylist">
						<option value="playlist" selected="selected">Select</option>
					</select>
					</td></tr>
					<tr><td>Playlist Open</td><td>
					<select id="playlistopen" class="skinlist">
						<option value="0" selected="selected">false</option>
						<option value="1">true</option>
					</select>
					</td></tr>
					<tr><td>Playlis Autoplay</td><td>
					<select id="playlistautoplay" class="skinlist">
						<option value="0" selected="selected">false</option>
						<option value="1">true</option>
					</select>
					</td></tr>
					<tr><td>Display Gallery</td><td>
					<select id="galleryoption">
						<option value="none" selected="selected">None</option>
						<option value="display">Display</option>
					</select>
					</td></tr>
					<tr><td>Gallery Name</td><td>
					<select id="playergallery">
						<option value="none" selected="selected">Select</option>
					</select>
					</td></tr>
					</table>
				</td>
				<td style="vertical-align:top;">
					<table id="hdw_skin">
					<tr>
					<th colspan="2">Skin Settings</th>
					</tr>
					<tr><td>Autoplay</td>	<td>
					<select id="autoplay" class="skinlist">
						<option value="1" selected="selected">true</option>
						<option value="0">false</option>
					</select>
					</td></tr>
					<tr><td>ControlBar</td><td>
					<select id="controlbar" class="skinlist">
						<option value="1" selected="selected">true</option>
						<option value="0">false</option>
					</select>
					</td></tr>
					<tr><td>Play Pause</td><td>
					<select id="playpause" class="skinlist">
						<option value="1" selected="selected">true</option>
						<option value="0">false</option>
					</select>
					</td></tr>
					<tr><td>Progress Bar</td><td>
					<select id="progressbar" class="skinlist">
						<option value="1" selected="selected">true</option>
						<option value="0">false</option>
					</select>
					</td></tr>
					<tr><td>Timer</td><td>
					<select id="timer" class="skinlist">
						<option value="1" selected="selected">true</option>
						<option value="0">false</option>
					</select>
					</td></tr>
					<tr><td>Share</td><td>
					<select id="share" class="skinlist">
						<option value="1" selected="selected">true</option>
						<option value="0">false</option>
					</select>
					</td></tr>
					<tr><td>Volume</td><td>
					<select id="volume" class="skinlist">
						<option value="1" selected="selected">true</option>
						<option value="0">false</option>
					</select>
					</td></tr>
					<tr><td>Fullscreen</td><td>
					<select id="fullscreen" class="skinlist">
						<option value="1" selected="selected">true</option>
						<option value="0">false</option>
					</select>
					</td></tr>
					<tr><td>Play Dock</td><td>
					<select id="playdock" class="skinlist">
						<option value="1" selected="selected">true</option>
						<option value="0">false</option>
					</select>
					</td></tr>
					<tr><td>Playlist</td><td>
					<select id="playlistdock" class="skinlist">
						<option value="1" selected="selected">true</option>
						<option value="0">false</option>
					</select>
					</td></tr>
					</table>
				</td>
				</tr>
				</table>
				
				<input id="saveplayer" type="button" Value="Save" />&nbsp;&nbsp;&nbsp;
				<input id="cancelplayer" type="button" Value="Cancel" />
				<input id="updateplayerbutton" type="button" Value="Update" />&nbsp;&nbsp;&nbsp;
				<input id="cancelplayer_update" type="button" Value="Cancel" />
				
			</div>
			<div id="playertotalcontent">
				
			</div>
			
			<div id="insertvideo">
				<input id="addvideo" type="button" value="Add Video"/><br/><br/>
				<table id="videodetails">
				<tr>
				<td style="width:30%;">Title <span style="color:red; font-weight:bold;">*</span></td>
				<td ><input id="videotitle" type="text"  /></td>
				</tr>
				<tr>
				<td >Type</td>
				<td >
				<select id="videotype" >
				<option value="direct" selected="selected">Direct Url</option>
				<option value="videoupload" >File Upload</option>
				<option value="youtube" >Youtube</option>
				<option value="vimeo" >Vimeo</option>
				<option value="dailymotion" >Dailymotion</option>
				<option value="rtmp" >RTMP</option>
				<option value="smil" >SMIL</option>
				<option value="lighttpd" >Lighttpd</option>
				<option value="bitgravity" >BitGravity</option>
				</select>
				</td>
				</tr>
				<tr>
				<td >Upload Video</td>
				<td>
				<form id="hdwupload_form" action="' . $GLOBALS["base_url"] . '/?q=hdw_fileupload" method="post" enctype="multipart/form-data">
				     <input class="hdwupload" type="file" size="60" id="hdwfile" name="hdwfile" />
				     <input class="hdwupload" type="submit" id="uploadbutton" value="Upload" />
				     <img id="uploadstatus" src="' . $GLOBALS["base_url"] . '/' . drupal_get_path('module', 'hdwplayer') . '/images/Circle32.gif" />
				</form>
				</td>
				</tr>
				<tr>
				<td >Url <span style="color:red; font-weight:bold;">*</span></td>
				<td ><input id="videourl" type="text" /></td>
				</tr>
				<tr>
				<td >Hd Url</td>
				<td ><input id="videohdurl" type="text" /></td>
				</tr>
				<tr>
				<td >Preview</td>
				<td ><input id="videopreview" type="text" /></td>
				</tr>
				<tr>
				<td >Thumb</td>
				<td ><input id="videothumb" type="text" /></td>
				</tr>
				<tr>
				<td >Streamer <span style="color:red; font-weight:bold;">*</span></td>
				<td ><input id="videostreamer" type="text" disabled="disabled" /></td>
				</tr>
				<tr>
				<td >token</td>
				<td ><input id="videotoken" type="text" disabled="disabled" /></td>
				</tr>
				<tr>
				<td >DVR</td>
				<td ><input id="videodvr" type="checkbox" disabled="disabled" /></td>
				</tr>
				<tr>
				<td >Playlist</td>
				<td >
				<select id="videoplaylist" >
				<option value="playlist" selected="selected">Select</option>
				</select>
				</td>
				</tr>
				<tr id="savebuttons">
				<td><input id="savevideo" type="button" value="Save" /></td>
				<td><input id="cancelvideo" type="button" value="Cancel" /></td>
				</tr>
				<tr id="updatebuttons">
				<td><input id="updatevideobutton" type="button" value="Update" /></td>
				<td><input id="cancelvideo_update" type="button" value="Cancel" /></td>
				</tr>
				</table>
			</div>
			<div id="videototalcontent">
				<input id="deletevideos" type="button" value="Delete" />
			</div>
			
			<div id="insertplaylist">				
				<input id="addplaylist" type="button" value="Add Playlist" /><br/><br/>
				<table id="playlistdetails">
				<tr>
				<td align="center">Name</td>
				<td ><input id="playlistname" type="text" /></td>
				</tr>
				<tr>
				<td><input id="saveplaylist" type="button" value="Save" /></td>
				<td><input id="cancelplaylist" type="button" value="Cancel" /></td>
				</tr>
				</table>
			</div>
			<div id="playlisttotalcontent">				
			</div>
			<div id="updateplaylistcontent">
				<table id="updateplaylist">
				<tr><td colspan="2" ><strong>Update Playlist</strong></td></tr>
				<tr>
				<td align="center" >Name</td>
				<td ><input id="updateplaylistname" type="text" /></td>
				</tr>
				<tr>
				<td><input id="updateplaylistbutton" type="button" value="Update" /></td>
				<td><input id="cancelupdateplaylist" type="button" value="Cancel" /></td>
				</tr>
				</table>
			</div>
			<div id="hdwgallerydiv">
				<input id="addgallery" type="button" value="Add Gallery"/><br/><br/>
				<table id="gallerydetails">
				<tr>
				<td >Name <span style="color:red; font-weight:bold;">*</span></td>
				<td ><input id="galleryname" type="text"  /></td>
				</tr>
				<tr>
				<td >No.of Rows </td>
				<td ><input id="galleryrows" type="text" placeholder="Default: 2" /></td>
				</tr>
				<tr>
				<td >No.of Columns </td>
				<td ><input id="gallerycolumns" type="text" placeholder="Default: 3" /></td>
				</tr>
				<tr>
				<td >Display Limit </td>
				<td ><input id="gallerylimit" type="text" placeholder="Default: 6" /></td>
				</tr>
				<tr>
				<td >Thumb Width </td>
				<td ><input id="gallerywidth" type="text" placeholder="Default: 100" /></td>
				</tr>
				<tr>
				<td >Thumb Height </td>
				<td ><input id="galleryheight" type="text" placeholder="Default: 70" /></td>
				</tr>
				<tr>
				<td >Thumb Space </td>
				<td ><input id="galleryspace" type="text" placeholder="Default: 2" /></td>
				</tr>
				<tr id="savegallerys">
				<td><input id="savegallery" type="button" value="Save" /></td>
				<td><input id="cancelgallery" type="button" value="Cancel" /></td>
				</tr>
				<tr id="updategallerys">
				<td><input id="updategallerybutton" type="button" value="Update" /></td>
				<td><input id="cancelgallery_update" type="button" value="Cancel" /></td>
				</tr>
				</table>
			</div>
			
			<div id="gallerytotalcontent">
			</div>
			
			<div id="documentationdiv">
				<div style="margin:30px; font-size:12.5px;">
				
				Creating Playlist:
				<ul>
				<li>Click Add Playlist Button.</li>
				<li>Enter the Playlist name and Save.</li>
				</ul><br>
				
				Creating Video:
				<ul>
				<li>Click Add Video Button.</li>
				<li>Enter the Video details, select playlist and Save.</li>
				</ul><br>
				
				Creating Gallery:
				<ul>
				<li>Click Add Gallery Button.</li>
				<li>Enter the Gallery details and Save.</li>
				</ul><br>
				
				Creating Player:
				<ul>
				<li>Click Add Player Button.</li>
				<li>Enter the Player details and Save.</li>
				</ul><br>
				
				Adding Playlist to Player:
				<ul>
				<li>Click Add Player Button.</li>
				<li>Select Playlist radio button under Video Settings.</li>
				<li>Select the Playlist and Save.</li>
				</ul><br>
				
				Adding Video to Player:
				<ul>
				<li>Click Add Player Button.</li>
				<li>Select Video radio button under Video Settings.</li>
				<li>Select Video Id and Save.</li>
				</ul><br>
				
				Adding Gallery to Player:
				<ul>
				<li>Click Add Player Button.</li>
				<li>Select Playlist under Video Settings tab.</li>
				<li>Set the Display Gallery from None to Display.</li>
				<li>Select the Gallery and Save.</li>
				</ul>
				<br><hr><br>

				Displaying the Player to Front-end:
				<ul>	
				<li>Select the Region for HDW Player Block from <a style="font-weight:bold; text-decoration:none;" target="_blank" href="'.$GLOBALS["base_url"].'/#overlay=%3Fq%3Dadmin%252Fstructure%252Fblock">here </a> and Save.</li>
				<li>Or</li>
				<li>Click the Structure button in drupal admin menubar.</li>
				<li>Click Blocks and set the Region to HDW Player and Save.</li>
				</ul><br>
				
				Setting Permission To Particular Block (Optional)
				<ul>
				<li>Select the respective block from <a style="font-weight:bold; text-decoration:none;" target="_blank" href="'.$GLOBALS["base_url"].'/#overlay=%3Fq%3Dadmin%252Fstructure%252Fblock">here </a>and Click Configure.</li>
				<li>Click the Roles tab under Visibility Settings.</li>
				<li>Set the roles as you wish to and Save.</li>
				</ul>
				
				</div>
			</div>
		</td>
		</tr>
	</table>
	';
 
 	drupal_add_js(drupal_get_path('module','hdwplayer') . "/JS/JS_Color_Picker/jscolor.js");
 	drupal_add_js(drupal_get_path('module','hdwplayer') . "/JS/hdwplayer.admin.js");
 	drupal_add_js(drupal_get_path('module','hdwplayer') . "/JS/jquery.form.js");
 	drupal_add_css(drupal_get_path('module','hdwplayer') . "/CSS/hdwplayer.admin.css");
 	
	//return system_settings_form($form);
	return $form;
}